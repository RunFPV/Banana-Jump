﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class SoundButton : MonoBehaviour
{
    AudioManager am;
    Image buttonImg;

    Sprite MUTE;
    Sprite SOUND;

    void Awake()
    {
        am = FindObjectOfType<AudioManager>();
        buttonImg = gameObject.GetComponent<Image>();

        MUTE = Resources.Load<Sprite>("Sprites/btnMuteInGame@0.5x");
        SOUND = Resources.Load<Sprite>("Sprites/btnSoundInGame@0.5x");

        if (am.soundsMuted)
        {
            buttonImg.sprite = MUTE;
        }
    }

    public void ToggleSounds()
    {
        if (am.soundsMuted)
        {
            // change sprite
            am.ToggleSounds(false);

            // Unmute sounds
            buttonImg.sprite = SOUND;
        }
        else
        {
            // Mute sounds
            am.ToggleSounds(true);

            // change sprite
            buttonImg.sprite = MUTE;
        }
    }
}
