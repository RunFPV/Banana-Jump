﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformGenerator : MonoBehaviour
{

    public GameObject[] platforms;

    Camera mainCamera;

    void Awake()
    {
        mainCamera = Camera.main;
    }

    public void GeneratePlatform(float playerDeltaPosition)
    {
        Vector3 camPos = mainCamera.transform.position;
        float camHeight = mainCamera.orthographicSize * 2;
        float edgeTopCam = mainCamera.orthographicSize;

        if (playerDeltaPosition != 0f)
        {
            edgeTopCam += playerDeltaPosition;
        }
        else
        {
            edgeTopCam += mainCamera.transform.position.y;
        }

        // Random pos on y axis, between 1/4 and 1/3 of screen height
        float randomPos = Random.Range(edgeTopCam - camHeight / 4, edgeTopCam - camHeight / 2);

        Vector3 newPos = new Vector3(camPos.x, randomPos, 0);

        int rngPlatformIndex = Random.Range(0, 3);

        Instantiate(platforms[rngPlatformIndex], newPos, Quaternion.identity);
    }
}
