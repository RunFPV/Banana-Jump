﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class HighScoreText : MonoBehaviour
{
    Text highScore;

    string HIGHSCORE_KEY = "HighScore";

    void Awake()
    {
        highScore = GetComponent<Text>();
        SetHighScore(GetHighScore());
    }

    public void SetHighScore(int value)
    {
        PlayerPrefs.SetInt(HIGHSCORE_KEY, value);
        highScore.text = "Best : " + value;
    }

    public int GetHighScore()
    {
        return PlayerPrefs.GetInt(HIGHSCORE_KEY);
    }

    public void ResetHighScore()
    {
        PlayerPrefs.DeleteKey(HIGHSCORE_KEY);
    }
}
