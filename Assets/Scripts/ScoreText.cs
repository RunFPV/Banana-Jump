﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ScoreText : MonoBehaviour
{
    Text score;

    void Awake()
    {
        score = GetComponent<Text>();
        SetScore(0);
    }

    public void SetScore(int value)
    {
        score.text = value.ToString();
    }
}
