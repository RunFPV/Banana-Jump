﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerProxy : MonoBehaviour
{
    GameManager gm;

    void Awake()
    {
        gm = FindObjectOfType<GameManager>();
    }

    public void GameOver()
    {
        gm.GameOver();
    }

    public void MainMenu()
    {
        gm.MainMenu();
    }

    public void RestartGame()
    {
        gm.RestartGame();
    }

    public void PauseGame()
    {
        gm.PauseGame();
    }

    public void ResumeGame()
    {
        gm.ResumeGame();
    }
}
