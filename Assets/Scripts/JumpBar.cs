﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JumpBar : MonoBehaviour
{

    public Slider slider;


    public void SetMaxJump(float jump)
    {
        slider.maxValue = jump;
        slider.value = jump;
    }

    public void SetJump(float jump)
    {
        slider.value = jump;
    }
}
