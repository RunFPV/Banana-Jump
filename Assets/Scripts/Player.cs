﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Rigidbody2D))]
public class Player : MonoBehaviour
{
    public JumpBar jumpBar;
    public PlatformGenerator platformGenerator;
    public ScoreText scoreText;
    public HighScoreText highScoreText;

    public float smoothMultiplier = .5f;
    public float forceMultiplier = 6f;
    public float maxJumpForce = 50f;
    public float camMoveSpeed = .5f;

    public static int score = 0;
    public static int highScore = 0;

    Platform platform;
    Camera mainCamera;

    Rigidbody2D rb;
    GameManager gm;
    AudioManager am;
    bool discharge;
    float charger;
    float jumpForce;
    bool isGrounded = false;
    bool camIsMoving = false;



    void Awake()
    {
        mainCamera = Camera.main;
    }


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        gm = FindObjectOfType<GameManager>();
        am = FindObjectOfType<AudioManager>();

        jumpBar.SetMaxJump(maxJumpForce);
        jumpBar.SetJump(0f);

        // Get HighScore
        highScore = highScoreText.GetHighScore();

        // Create first platform
        platformGenerator.GeneratePlatform(0f);

    }

    // Update is called once per frame
    void Update()
    {
        // When pointer is on UI button, return true.
        bool isUITarget = EventSystem.current.IsPointerOverGameObject();

        if (!isUITarget && isGrounded && !camIsMoving)
        {
            // If pressing Space, charge the variable charger using the Time it's being pressed.
            if (Input.GetMouseButton(0))
            {
                charger += Time.deltaTime;
                jumpForce = smoothMultiplier * Mathf.Exp((forceMultiplier * charger)) * 10;
                jumpBar.SetJump(jumpForce);

                if (jumpForce > maxJumpForce)
                {
                    jumpForce = maxJumpForce;
                    jumpBar.SetJump(jumpForce);
                }
            }

            // On release, set the boolean 'discharge' to true.
            if (Input.GetMouseButtonUp(0))
            {
                discharge = true;
            }
        }

        // Game ended if player enter in collision with top or bottom camera edge
        Vector3 camPos = mainCamera.WorldToViewportPoint(transform.position);
        if ((camPos.y > 1f || camPos.y < 0f) && !GameManager.gameIsOver)
        {
            Destroy(gameObject);
            gm.GameOver();
        }
    }


    // Using fixed update since we are dealing with Unity's Physics
    void FixedUpdate()
    {
        // On (discharge == true)
        if (discharge)
        {
            // Set jump force and give new velocity
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);

            // Reset discharge and charger values
            discharge = false;
            charger = 0f;
            jumpBar.SetJump(0f);

            // Play Jump sound
            int i = Random.Range(1, 4);
            am.Play("Jump" + i);
        }

    }


    void OnCollisionEnter2D(Collision2D collision)
    {
        // When player hits sometihing from above
        if (collision.relativeVelocity.y > 0f)
        {
            isGrounded = true;

            // If object is a platform
            GameObject collidedObject = collision.gameObject;
            if (collidedObject.CompareTag("Platform"))
            {
                platform = collidedObject.GetComponent<Platform>();

                // Avoid "jumping/jercking" behaviour
                transform.SetParent(platform.transform);

                // Platform not moving, so first time player is on it.
                if (!platform.GetMoveDown())
                {
                    // Add +1 to score
                    score++;
                    scoreText.SetScore(score);

                    // If score better than HighScore
                    if (score > highScore)
                    {
                        highScore = score;
                        highScoreText.SetHighScore(highScore);
                    }

                    // More score = more difficulty
                    platform.speed = platform.speed * score / 10;

                    // Const used to create new platform and to move camera
                    Vector3 camPos = mainCamera.transform.position;
                    float camHeight = mainCamera.orthographicSize * 2;
                    float playerDeltaPosition = transform.position.y + camHeight / 4;

                    // Create a new plateform before move cam
                    platformGenerator.GeneratePlatform(playerDeltaPosition);

                    // Move smoothy camera
                    Vector3 newPos = new Vector3(camPos.x, playerDeltaPosition, mainCamera.transform.position.z);
                    StartCoroutine(MoveToPosition(camPos, newPos, camMoveSpeed));
                }
            }
        }
    }


    void OnCollisionExit2D(Collision2D collision)
    {
        isGrounded = false;
        transform.DetachChildren();
    }


    IEnumerator MoveToPosition(Vector3 startingPos, Vector3 newPos, float time)
    {
        float elapsedTime = 0;
        while (elapsedTime < time)
        {
            camIsMoving = true;
            mainCamera.transform.position = Vector3.Lerp(startingPos, newPos, (elapsedTime / time));
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        camIsMoving = false;

        // Moving down platform after camera has ended moving
        platform.SetMoveDown(true);
    }

    public bool GetGrounded()
    {
        return isGrounded;
    }
}
