﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public GameObject pausedMenuUI;
    public GameObject gameOverMenuUI;

    public static bool gameIsPaused = false;
    public static bool gameIsOver = false;

    AudioManager am;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        am = FindObjectOfType<AudioManager>();
    }

    public void GameOver()
    {
        if (!gameIsOver)
        {
            gameIsOver = true;
            Time.timeScale = 0f;

            // Show best and current score
            Text[] texts = gameOverMenuUI.GetComponentsInChildren<Text>();
            if (texts[0].name == "ScoreText")
            {
                texts[0].text = "SCORE : " + Player.score;
                texts[1].text = "BEST : " + Player.highScore;
            }
            else
            {
                texts[0].text = "BEST : " + Player.highScore;
                texts[1].text = "SCORE : " + Player.score;
            }


            // Print menu end game (UI)
            gameOverMenuUI.SetActive(true);
            am.Stop("Theme");
            am.Play("End");
        }
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MenuScene");

        gameOverMenuUI.SetActive(false);
        pausedMenuUI.SetActive(false);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("GameScene");

        gameOverMenuUI.SetActive(false);
        pausedMenuUI.SetActive(false);

        Time.timeScale = 1f;
        gameIsOver = false;
        gameIsPaused = false;
        Player.score = 0;
        am.Play("Theme");
    }

    public void PauseGame()
    {
        gameIsPaused = true;
        pausedMenuUI.SetActive(true);
        Time.timeScale = 0f;
    }

    public void ResumeGame()
    {
        gameIsPaused = false;
        pausedMenuUI.SetActive(false);
        Time.timeScale = 1f;
    }

}
